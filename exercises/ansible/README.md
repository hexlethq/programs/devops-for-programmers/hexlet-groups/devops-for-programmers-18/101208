# Ansible

Настроим Vagrant-окружение используя [Ansible Provisioning](https://www.vagrantup.com/docs/provisioning/ansible) и запустим внутри него веб-сервер Caddy, который будет обрабатывать запросы для микрофреймворка Fastify.

## Ссылки

* [Ansible Documentation](https://docs.ansible.com/ansible/latest/index.html)
* [Caddy](https://github.com/caddyserver/caddy)

## Задачи

1. Ansible: Установите git, make, postgres
2. Ansible: Залейте в postgres базу данных [pg-dump-example](https://github.com/hexlet-components/pg-dump-example). Для работы Ansible-модуля postgresql_user нужно установить пакет [psycopg2](https://docs.ansible.com/ansible/2.8/modules/postgresql_user_module.html#requirements) 
3. Ansible: Установите [Node.js](https://github.com/nodesource/distributions#installation-instructions) и сервер [Caddy](https://caddyserver.com/docs/install) с помощью [shell-скриптов](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/script_module.html).
4. Ansible: Выставьте сервер наружу на портах 8080 для HTTP и 4430 для HTTPS.
5. Вручную: Установите [fastify](https://github.com/fastify/fastify#quick-start). Запустите Fastify и сделайте так, чтобы он был доступен снаружи Vagrant через Caddy.

```sh
vagrant ssh
cd /vagrant
mkdir ~/node_modules
ln -s ~/node_modules node_modules # Линкуем node_modules внутрь виртуальной машины.
# Простой способ обойти ограничение на создание символических ссылок в общей директории.
caddy reload # подгружаем наш Caddyfile
npm init -y fastify
npm install
FASTIFY_ADDRESS=0.0.0.0 npm run dev # проверяем что все работает
# open localhost:8080
```

Убедитесь в том что все задачи выполняются идемпотентно.

В результате у вас должен быть *Vagrantfile* в котором пробрасываются порты и выполняется playbook с помощью Ansible Provisioning, а также *Caddyfile* в котором настроена обработка запросов Fastify.

## Подсказки

* [allow_world_readable_tmpfiles](https://docs.ansible.com/ansible/2.4/intro_configuration.html#allow-world-readable-tmpfiles)
